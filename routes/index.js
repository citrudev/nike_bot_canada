let proxy = require('../controllers/proxy.controller')
let task = require('../controllers/task.controller')
let account = require('../controllers/account.controller')
let config = require('../config/configuration')

module.exports = (app) => {
  let auth = app.locals.auth

  app.get('/', auth.userHasSession, (req, res) => {
    res.render('login')
  })

  app.get('/signup', auth.userHasSession, (req, res) => {
    res.render('signup')
  })

  app.get('/bot', (req, res) => {
    res.render('bot')
  })

  app.get('/logout', auth.logout)

  app.post('/', auth.userHasSession, auth.login)

  app.post('/signup', auth.register)

  //  *********************************************API*********************************************/
  app.post('/proxies', proxy.create)// create one

  app.get('/proxies', proxy.findall)// get all

  app.get('/proxies/:id', proxy.findone)// get one

  app.delete('/proxies/:id', proxy.delete)// delete

  app.put('/proxies/:id', proxy.update)// update

  // -----------------------------------------------------------------------------------------------

  app.post('/accounts', account.create)// create one

  app.get('/accounts', account.findall)// get all

  app.get('/accounts/:id', account.findone)// get one

  app.delete('/accounts/:id', account.delete)// delete

  app.put('/accounts/:id', account.update)// update

  // -----------------------------------------------------------------------------------------------

  app.post('/tasks', task.create)// create one

  app.get('/tasks', task.findall)// get all

  app.get('/tasks/:id', task.findone)// get one

  app.delete('/tasks/:id', task.delete)// delete

  app.put('/tasks/:id', task.update)// update

  // ---------------------------------------------------------------------------------------------------
  app.post('/configTwitter', config.updateTwitterConfiguration)

  app.get('/configTwitter', config.readTwitterConfiguration)
  // -----------------------------------------------------------------------------------------------

  app.all('/*', (req, res) => {
    res.render('404')
  })
}
