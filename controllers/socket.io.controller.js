let taskManager = require('../lib/task.manager')

module.exports = (io, twitter) => {
  io.on('connection', function (socket) {
    socket.on('task:start', function (data) {
      console.log(data)
      taskManager.start(data, (task) => {
        task.on('message', (status) => {
          socket.emit('task:notification', {id: status.id, progress: status.progress, status: status.status})
          if (data.twitterHandle && status.code === 200) {
            twitter.post('direct_messages/new', {screen_name: data.twitterHandle, text: `${status.status}`}, function (err, data, response) {
              if (err) {
                console.log(err)
              }
            })
          }
        })
      })
    })

    socket.on('task:stop', function (data) {
      console.log(data)
      taskManager.stop(data.id)
    })

    socket.on('task:stop.all', function (data) {
      taskManager.stopAll((status) => {
        socket.emit('notification:general', status)
      })
    })
  })
}
