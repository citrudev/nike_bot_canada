var Account = require('../models/account.model')

module.exports.create = (req, res) => {
  let newAccount = new Account()
  newAccount.name = req.body.name
  newAccount.username = req.body.username
  newAccount.password = req.body.password
  newAccount.save((err, account) => {
    if (!err) {
      return res.status(200).json(account)
    } else {
      return res.status(500).json(err)
    }
  })
}

module.exports.findall = (req, res) => {
  Account.find({}, (err, accounts) => {
    if (!err) {
      if (!accounts) {
        return res.status(404).json(accounts)
      }
      return res.status(200).json(accounts)
    } else {
      return res.status(500).json(err)
    }
  })
}

module.exports.findone = (req, res) => {
  Account.findOne({_id: req.params.id}, (err, account) => {
    if (!err) {
      if (!account) {
        return res.status(404).json(account)
      }
      return res.status(200).json(account)
    } else {
      return res.status(500).json(err)
    }
  })
}

module.exports.delete = (req, res) => {
  Account.findByIdAndRemove({_id: req.params.id}, (err, account) => {
    if (!err) {
      return res.status(200).json(account)
    } else {
      return res.status(500).json(err)
    }
  })
}

module.exports.update = (req, res) => {
  Account.findByIdAndUpdate(req.params.id, { $set: req.body }, (err, account) => {
    if (!err) {
      return res.status(200).json(account)
    } else {
      return res.status(500).json(account)
    }
  })
}
