var Task = require('../models/task.model.js')
const uuidv4 = require('uuid/v4')

module.exports.create = (req, res) => {
  console.log(req.body)
  let newTask = new Task()
  newTask.name = req.body.name
  newTask.type = req.body.type || 'nike'
  newTask.twitterHandle = req.body.twitterHandle
  newTask.productUrl = req.body.productUrl
  newTask.sizes = req.body.sizes.split(',')
  newTask.username = req.body.username
  newTask.password = req.body.password
  newTask.session = uuidv4()
  newTask.save((err, task) => {
    if (!err) {
      return res.status(200).json(task)
    } else {
      return res.status(500).json(err)
    }
  })
}

module.exports.findall = (req, res) => {
  Task.find({}, (err, task) => {
    if (!err) {
      if (!task) {
        return res.status(404).json(task)
      }
      return res.status(200).json(task)
    } else {
      return res.status(500).json(err)
    }
  })
}

module.exports.findone = (req, res) => {
  Task.findOne({_id: req.params.id}, (err, task) => {
    if (!err) {
      if (!task) {
        return res.status(404).json(task)
      }
      return res.status(200).json(task)
    } else {
      return res.status(500).json(err)
    }
  })
}

module.exports.delete = (req, res) => {
  Task.findByIdAndRemove({_id: req.params.id}, (err, task) => {
    if (!err) {
      return res.status(200).json(task)
    } else {
      return res.status(500).json(err)
    }
  })
}

module.exports.update = (req, res) => {
  Task.findByIdAndUpdate(req.params.id, { $set: req.body }, (err, task) => {
    if (!err) {
      return res.status(200).json(task)
    } else {
      return res.status(500).json(err)
    }
  })
}
