var Proxy = require('../models/proxy.model')

module.exports.create = (req, res) => {
  let newProxy = new Proxy()
  newProxy.ip = req.body.ip
  newProxy.port = req.body.port
  newProxy.user = req.body.user
  newProxy.password = req.body.password
  newProxy.save((err, ip) => {
    if (!err) {
      return res.status(200).json(ip)
    } else {
      return res.status(500).json(err)
    }
  })
}

module.exports.findall = (req, res) => {
  Proxy.find({}, (err, proxies) => {
    if (!err) {
      if (!proxies) {
        return res.status(404).json(proxies)
      }
      return res.status(200).json(proxies)
    } else {
      return res.status(500).json(err)
    }
  })
}

module.exports.findone = (req, res) => {
  Proxy.findOne({_id: req.params.id}, (err, proxy) => {
    if (!err) {
      if (!proxy) {
        return res.status(404).json(proxy)
      }
      return res.status(200).json(proxy)
    } else {
      return res.status(500).json(err)
    }
  })
}

module.exports.delete = (req, res) => {
  Proxy.findByIdAndRemove({_id: req.params.id}, (err, ip) => {
    if (!err) {
      return res.status(200).json(ip)
    } else {
      return res.status(500).json(err)
    }
  })
}

module.exports.update = (req, res) => {
  Proxy.findByIdAndUpdate(req.params.id, { $set: req.body }, (err, proxy) => {
    if (!err) {
      return res.status(200).json(proxy)
    } else {
      return res.status(500).json(proxy)
    }
  })
}
