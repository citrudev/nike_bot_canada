var jsonfile = require('jsonfile')
var taskManager = require('../lib/task.manager')
module.exports.updateTwitterConfiguration = (req, res) => {
  jsonfile.writeFile(`${process.cwd()}/config/twitterConfig.json`, req.body, {spaces: 2}, function (err, data) {
    if (!err) {
      res.status(200).json(data)
    } else {
      res.status(500).json(err)
    }
  })
}

module.exports.readTwitterConfiguration = (req, res) => {
  jsonfile.readFile(`${process.cwd()}/config/twitterConfig.json`, function (err, data) {
    if (err) {
      res.status(200).json(data)
    } else {
      res.status(500).json(data)
    }
  })
}

module.exports.readTaskTypes = (req, res) => {
  res.status(200).json(taskManager.taskTypes)
}
