var jsonfile = require('jsonfile')

module.exports = (Twit) => {
  const config = jsonfile.readFileSync(`${process.cwd()}/config/twitterConfig.json`)
  if (config['TWITTER_CONSUMER_KEY'] && config['TWITTER_CONSUMER_SECRET'] && config['TWITTER_ACCESS_TOKEN'] && config['TWITTER_ACCESS_TOKEN_SECRET']) {
    return new Twit({
      consumer_key: config['TWITTER_CONSUMER_KEY'],
      consumer_secret: config['TWITTER_CONSUMER_SECRET'],
      access_token: config['TWITTER_ACCESS_TOKEN'],
      access_token_secret: config['TWITTER_ACCESS_TOKEN_SECRET'],
      timeout_ms: 60 * 1000 // optional HTTP request timeout to apply to all requests.
    })
  } else {
    return null
  }
}
