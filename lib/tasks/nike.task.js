let Nightmare = require('nightmare')
let sizeList = require('../sizes')
let vo = require('vo')
let nightmareXpath = require('nightmare-xpath-event')
let wishList = `https://store.nike.com/ca/en_gb/?l=shop,mylocker`
const screenshotSelector = require('nightmare-screenshot-selector')

// plugins for nightmare
Nightmare.action('touch', nightmareXpath.touch)
Nightmare.action('clickByXpath', nightmareXpath.clickByXpath)
Nightmare.action('screenshotSelector', screenshotSelector)

// retry if fail
module.exports = function retry (data) {
  require('../configure.nightmare')(Nightmare, data.session, true, function (nightmare) {
    vo(function * (data) {
      process.send({status: `task started`, id: data._id, code: 100, progress: 20, data: null})
      let sizes = data.sizes

      yield nightmare.goto(data.productUrl)

      console.log('about to wait for buytools form')
      while (!((yield nightmare.wait('form[id*="buyTools"]').exists('form[id*="buyTools"]')) || (yield nightmare.wait('form[id*="buyTools"]').exists('form[id*="buyTools"]')))) {
        console.log('waiting for buytools form')
        yield nightmare.wait(1000)
      }

      process.send({status: `bypassed countdown`, id: data._id, code: 100, progress: 70, data: null})
      console.log(`Bypassed countdown`)

      if (yield nightmare.exists('form[id*="buyTools"]')) {
        console.log('buy tools')
        for (var i in sizes) {
          console.log(sizeList[sizes[i]])
          var exists = yield nightmare.exists(sizeList[sizes[i]])
          if (exists) {
            yield nightmare.click(sizeList[sizes[i]])
            console.log('clicked on size')
            yield nightmare.clickByXpath('//*[@id="buyTools"]/div[2]/button[2]')
            console.log('clicked on add to wish list')
            yield nightmare.wait('[id="f95643c0-aa8b-430a-ad6a-a1d76e7f07dd"]')
            console.log('wait for facebook login button')
            yield nightmare.clickByXpath('//*[@id="f95643c0-aa8b-430a-ad6a-a1d76e7f07dd"]')
            console.log('clicked on facebook login')
            yield nightmare.goto(wishList)
            console.log('go to favorites')
            yield nightmare.wait('#mylocker > div.exp-page-content > div.items-section > div.content-header.nsg-bg--dark-grey.nsg-font-family--platform.edf-font-size--xlarge > div.content-header-content.nsg-text--white')

            while (!(yield nightmare.evaluate(function () {
              var cartNum = document.querySelector('#gen-nav-commerce-header > header > nav > section.d-sm-b > div > div.l-mobile-nav.d-lg-h.pt2-sm.pb2-sm > div > div > a > span')
              console.log(cartNum.innerHTML.toString().split(' ')[0])
              return Number(cartNum.innerHTML.toString().split(' ')[0]) < 1
            }))) {
              yield nightmare.clickByXpath('//*[@id="mylocker"]/div[2]/div[1]/div[2]/div[2]/div[2]/div[3]/div[3]/button[2]')
              console.log('click on add to cart')
              yield nightmare.wait(500)
            }
            break
          }
        }
      }
      return yield nightmare.end()
    })(data, function (err, result) {
      if (!err) {
        process.exit()
      } else {
        console.log(err)
        process.send({status: `operation failed: ${err} for ${data.name}`, code: 205, progress: 0, id: data._id})
        retry(data)
      }
    })
  })
}
