let Nightmare = require('nightmare')
let Xvfb = require('xvfb')
let vo = require('vo')
let facebokUrl = `https://www.facebook.com/`
const xvfb = new Xvfb({ reuse: true })
const nikeTask = require('./nike.task')

function facebookSession (data) {
  require('../configure.nightmare')(Nightmare, data.session, true, function (nightmare) {
    vo(function * (data) {
      yield nightmare.goto(facebokUrl)
      yield nightmare.wait('#u_0_a > div:nth-child(1) > div:nth-child(1) > div > a > span > span')
      var session = yield nightmare.exists('#u_0_a > div:nth-child(1) > div:nth-child(1) > div > a > span > span')
      yield nightmare.end()
      return session
    })(data, function (err, session) {
      if (!err) {
        if (session) {
          nikeTask(data)
        } else {
          console.log('please log into your facebook account')
          process.send({status: `please login to your facebook account for task ${data.name}`, code: 205, progress: 0, id: data._id})
        }
        process.exit()
      } else {
        console.log(err)
        process.send({status: `operation failed (${err}) for task ${data.name}`, code: 205, progress: 0, id: data._id})
      }
    })
  })
}

process.on('message', (data) => {
  // xvfb for linux server
  // xvfb.startSync()
  facebookSession(data)
})
