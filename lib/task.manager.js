let instance = null
let tasks = require('../models/task.model')
const cp = require('child_process')

let Types = {
  'nike': `${__dirname}/tasks/session.facebook.nike.task.js`,
  'adidas': `${__dirname}/tasks/adidas.task.js`
}

let Tasks = {}

class TaskManager {
  constructor () {
    if (!instance) {
      instance = this
    }
    return instance
  }

  start (data, cb) {
    if (!Tasks[data.id]) {
      tasks.findById(data.id, function (err, task) {
        if (!err) {
          Tasks[data.id] = cp.fork(Types[data.type])
          Tasks[data.id].send(task)
          cb(Tasks[data.id])
        }
      })
    } else {
      cb(null)
    }
  }

  exists (id) {
    if (Tasks[id]) {
      return true
    }
    return false
  }

  onMessage (id, cb) {
    if (Tasks[id]) {
      Tasks[id].on('message', (msg) => {
        cb(msg)
      })
    }
  }

  sendMessage (id, data) {
    if (Tasks[id]) {
      Tasks[id].send(data)
    }
  }

  stop (id) {
    Tasks[id].kill('SIGINT')
  }

  killAll () {
    for (var id in Tasks) {
      Tasks[id].kill()
    }
  }
}

module.exports = new TaskManager()
module.exports.taskType = Object.keys(Types)
