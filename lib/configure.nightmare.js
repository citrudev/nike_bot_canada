var proxyRotation = require('./proxy.rotation')
module.exports = function (Nightmare, session, show, cb) {
  proxyRotation.randomProxy((proxy) => {
    console.log(proxy)
    if (proxy) {
      var nightmareWithProxy = new Nightmare(
        {
          typeInterval: 300,
          show: show,
          maxAuthRetries: 30,
          pollInterval: 200,
          waitTimeout: 3600000,
          electronPath: require('electron'),
          openDevTools: true,
          switches: {
            'ignore-certificate-errors': true,
            'proxy-server': proxy.ip + ':' + proxy.port
          },
          webPreferences: {
            webSecurity: false,
            partition: `persist:${session}`,
            allowRunningInsecureContent: true
          }
        })
      cb(nightmareWithProxy.useragent(proxyRotation.genUseragent()).authentication(proxy.user || '', proxy.password || ''))
    } else {
      var nightmareWithNoProxy = new Nightmare(
        {
          typeInterval: 300,
          show: show,
          maxAuthRetries: 30,
          pollInterval: 200,
          waitTimeout: 3600000,
          electronPath: require('electron'),
          openDevTools: true,
          switches: {
            'ignore-certificate-errors': true
          },
          webPreferences: {
            webSecurity: false,
            partition: `persist:${session}`,
            allowRunningInsecureContent: true
          }
        })
      cb(nightmareWithNoProxy.useragent(proxyRotation.genUseragent()))
    }
  })
}
