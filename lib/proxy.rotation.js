const mongoose = require('mongoose')
let Proxy = require('../models/proxy.model')
mongoose.connect(process.env.MONGODB)

let userAgents = [
  'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.135 Safari/537.36 Edge/12.246',
  'Mozilla/5.0 (X11; CrOS x86_64 8172.45.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.64 Safari/537.36',
  'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_2) AppleWebKit/601.3.9 (KHTML, like Gecko) Version/9.0.2 Safari/601.3.9',
  'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.111 Safari/537.36',
  'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:15.0) Gecko/20100101 Firefox/15.0.1'
]
module.exports = function () {}

module.exports.genUseragent = function (_name) {
  return userAgents[Math.floor((Math.random() * userAgents.length))]
}

module.exports.randomProxy = function (cb) {
  Proxy.find({}, function (err, proxies) {
    if (!err) {
      if (proxies) {
        cb(proxies[Math.floor((Math.random() * proxies.length))])
      } else {
        cb(null)
      }
    } else {
      cb(null)
    }
  })
}
