let LocalStrategy = require('passport-local').Strategy
let User = require('../models/user.model')

module.exports = (passport) => {
  //  passport serializeUser
  passport.serializeUser((user, done) => {
    done(null, user._id)
  })
  // deserialize User
  passport.deserializeUser((id, done) => {
    User.findById(id, (err, user) => {
      done(err, user)
    })
  })
  // --------------------------------------------------------
  // configure passport LocalStrategy used to register user
  passport.use('local-register', new LocalStrategy({
    usernameField: 'email',
    passwordField: 'pass',
    passReqToCallback: true
  },

  function (req, email, pass, done) {
    process.nextTick(function () {
      User.findOne({email: email}, function (err, user) {
        if (err) return done(err)
        if (user) return done(null, false, {message: 'email already exist'})
        var newUser = new User()
        newUser.email = email
        newUser.password = newUser.generateHash(pass)
        newUser.save((err, user) => {
          if (err) return done(err)
          return done(null, user)
        })
      })
    })
  }
  ))

  // configure passport LocalStrategy used to authenticate
  passport.use('local-login', new LocalStrategy({
    usernameField: 'email',
    passwordField: 'pass',
    passReqToCallback: true
  }, function (req, email, pass, done) {
    User.findOne({email: email}, function (err, user) {
      if (err) return done(err)
      if (!user) return done(null, false, {message: 'Incorrect username.'})
      if (!user.validPassword(pass)) return done(null, false, {message: 'Incorrect password.'})
      return done(null, user)
    })
  }))

  // register a user
  let register = passport.authenticate('local-register', {
    successRedirect: '/', // redirect to login page
    failureRedirect: '/signup' // redirect to signup page
  })

  // authenticate a user
  let login = passport.authenticate('local-login', {
    successRedirect: '/bot',
    failureRedirect: '/' // redirect to login page
  })

  // test authentication
  let isAuthenticated = (req, res, next) => {
    if (!req.isAuthenticated()) return res.redirect('/')
    if (!req.user) {
      req.logout()
      return res.redirect('/')
    }
    // if user not authenticated redirect to login page
    next()
  }

  // Authenticate Api
  let isAuthenticatedApi = (req, res, next) => {
    if (!req.isAuthenticated()) return res.status(401).json({})
    if (!req.user) {
      return res.status(401).json({})
    }
    next()
  }

  let userHasSession = (req, res, next) => {
    if (req.user) {
      return res.redirect('/bot')
    }
    next()
  }

  let logout = function (req, res) {
    req.logout()
    res.redirect('/')
  }

  return {
    isAuthenticated: isAuthenticated,
    isAuthenticatedApi: isAuthenticatedApi,
    userHasSession: userHasSession,
    register: register,
    login: login,
    logout: logout
  }
}
