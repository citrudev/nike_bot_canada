var socket = io()
var status = {}
loadProxies()

socket.on('task:notification', function (status) {
  console.log(status)
  $(`[data-status=${status.id}]`).attr('aria-valuenow', status.progress)
  $(`[data-status=${status.id}]`).text(status.status)
})

function deleteTask (id) {
  $.ajax({
    url: `/tasks/${id}`,
    dataType: 'json',
    type: 'delete',
    contentType: 'application/json',
    success: function (data) {
      loadTasks()
      $('#alert').addClass('alert alert-warning').text('task deleted').fadeIn()
    },
    error: function (jqXhr, textStatus, errorThrown) {
      console.log(jqXhr)
      $('#alert').addClass('alert alert-danger').text('something went wrong with deleting task').fadeIn()
      $('#proxy-ip-addr').val('')
      $('#proxy-port').val('')
      $('#proxy-user').val('')
      $('#proxy-password').val('')
    }
  })
}

function startTask (id, type) {
  socket.emit('task:start', {id: id, type: type})
  alert('task started')
}

function stopTask (id) {
  socket.emit('task:stop', {id: id})
  alert('Attemping to stop task')
  $(`#${id}.progress-bar.progress-bar-success.status`).attr(`aria-valuenow`, 0)
}

function addtask () {
  $.ajax({
    url: '/tasks',
    dataType: 'json',
    type: 'post',
    contentType: 'application/json',
    data: JSON.stringify({ 'name': $('#task-name').val(), 'twitterHandle': $('#task-twitter').val(), 'productUrl': $('#task-url').val(), 'sizes': $('#task-sizes').val(), 'username': $('#task-user').val(), 'password': $('#task-password').val() }),
    processData: false,
    success: function (data, textStatus, jQxhr) {
      loadTasks()
      $('#alert').addClass('alert alert-success').text('new task added').fadeIn()
      $('#proxy-ip-addr').val('')
      $('#proxy-port').val('')
      $('#proxy-user').val('')
      $('#proxy-password').val('')
    },
    error: function (jqXhr, textStatus, errorThrown) {
      $('#alert').addClass('alert alert-danger').text('something went wrong').fadeIn()
      $('#proxy-ip-addr').val('')
      $('#proxy-port').val('')
      $('#proxy-user').val('')
      $('#proxy-password').val('')
    }
  })
}

function loadProxies () {
  $.ajax({
    url: '/proxies',
    dataType: 'json',
    type: 'get',
    contentType: 'application/json',
    success: function (data) {
      $('#proxy-panel').text('')
      for (var i in data) {
        $('#proxy-panel').append(`<div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title">Proxy</h3>
        </div>
        <div class="panel-body">
          <div class="row">
            <div class="col-md-9">
                <span class="btn alert-danger">${data[i].ip}</span>
                <span class="btn alert-danger">${data[i].port}</span>
                <span class="btn alert-danger">${data[i].user}</span>
                <span class="btn alert-danger">${data[i].password}</span>
            </div>
            <div class="col-md-2">
              <button onclick="deleteProxy('${data[i]._id}')" class="btn btn-primary delete-proxy"><i class="far fa-trash-alt"></i></button>
              </div>
            <div class="col-md-1">
                
            </div>
          </div>   
        </div>
      </div>`)
      }
    },
    error: function (jqXhr, textStatus, errorThrown) {
      $('#alert').addClass('alert alert-danger').text('something went wrong loading proxies').fadeIn()
      $('#proxy-ip-addr').val('')
      $('#proxy-port').val('')
      $('#proxy-user').val('')
      $('#proxy-password').val('')
    }
  })
}

function loadAccounts () {
  $.ajax({
    url: '/accounts',
    dataType: 'json',
    type: 'get',
    contentType: 'application/json',
    success: function (data) {
      $('#accounts-panel').text('')
      for (var i in data) {
        $('#accounts-panel').append(`<div  class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title">${data[i].name}</h3>
        </div>
        <div class="panel-body">
          <div class="row">
            <div class="col-md-9">
                <span class="btn alert-danger">Username:&nbsp${data[i].username}</span>
                <span class="btn alert-danger">Password:&nbsp${data[i].password}</span>
            </div>
            <div class="col-md-2">
              <button onclick="deleteAccount('${data[i]._id}')" class="btn btn-primary delete-proxy"><i class="far fa-trash-alt"></i></button>
              </div>
            <div class="col-md-1">
                
            </div>
          </div>   
        </div>
      </div>`)
      }
    },
    error: function (jqXhr, textStatus, errorThrown) {
      $('#alert').addClass('alert alert-danger').text('something went wrong loading accounts').fadeIn()
      $('#proxy-ip-addr').val('')
      $('#proxy-port').val('')
      $('#proxy-user').val('')
      $('#proxy-password').val('')
    }
  })
}

function loadTasks () {
  $.ajax({
    url: '/tasks',
    dataType: 'json',
    type: 'get',
    contentType: 'application/json',
    success: function (data) {
      console.log(data)
      $('#tasks-panel').text('')
      for (var i in data) {
        $('#tasks-panel').append(`<div id="${data[i]._id}" class="panel panel-default">
        <div class="panel-heading">
              <h3 class="panel-title">${data[i].name}</h3>
            </div>
        <div class="panel-body">
          <div class="row">
            <div class="col-md-10">
                <table class="table table-condensed">
                    <thead>
                        <tr>
                          <th>Task Name</th>
                          <th>Twitter handle</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td><span class="btn alert-danger">${data[i].name}</span></td>
                          <td><span class="btn alert-danger">${data[i].twitterHandle}</span></td>
                        </tr>
                    </tbody>
                </table>
                <div>
                  Product Url:
                  <span class="badge">${data[i].productUrl}</span>
                </div>
                <div>
                  Sizes:
                  <span class="badge">${data[i].sizes.join(' | ')}</span>
                </div>
                    <br/>
                    <div class="progress">
                        <div class="progress-bar progress-bar-success status" data-status=${data[i]._id} role="progressbar" aria-valuenow="10"
                        aria-valuemin="0" aria-valuemax="100">
                        
                        </div>
                      </div>
            </div>
            <div class="col-md-2">
                <button onclick="deleteTask('${data[i]._id}')" style="margin: 5px" class="btn btn-primary"><i class="far fa-trash-alt"></i></button>
                &nbsp
                <button onclick="startTask('${data[i]._id}', '${data[i].type}')" style="margin: 5px" class="btn btn-primary"><i class="fas fa-play"></i></button>
                &nbsp
                <button onclick="stopTask('${data[i]._id}')" style="margin: 5px" class="btn btn-primary"><i class="fas fa-stop-circle"></i></button>
                &nbsp
              </div>
      
                
            </div>
          </div>   
        </div>`)
      }
    },
    error: function (jqXhr, textStatus, errorThrown) {
      $('#alert').addClass('alert alert-danger').text('something went wrong loading proxies').fadeIn()
      $('#proxy-ip-addr').val('')
      $('#proxy-port').val('')
      $('#proxy-user').val('')
      $('#proxy-password').val('')
    }
  })
}

function deleteProxy (id) {
  $.ajax({
    url: `/proxies/${id}`,
    dataType: 'json',
    type: 'delete',
    contentType: 'application/json',
    success: function (data) {
      loadProxies()
      $('#alert').addClass('alert alert-warning').text('proxy removed').fadeIn()
    },
    error: function (jqXhr, textStatus, errorThrown) {
      $('#alert').addClass('alert alert-danger').text('something went wrong deleting proxies').fadeIn()
      $('#proxy-ip-addr').val('')
      $('#proxy-port').val('')
      $('#proxy-user').val('')
      $('#proxy-password').val('')
    }
  })
}

function deleteAccount (id) {
  $.ajax({
    url: `/accounts/${id}`,
    dataType: 'json',
    type: 'delete',
    contentType: 'application/json',
    success: function (data) {
      loadAccounts()
      $('#alert').addClass('alert alert-warning').text('account removed').fadeIn()
    },
    error: function (jqXhr, textStatus, errorThrown) {
      $('#alert').addClass('alert alert-danger').text('something went wrong deleting proxies').fadeIn()
      $('#proxy-ip-addr').val('')
      $('#proxy-port').val('')
      $('#proxy-user').val('')
      $('#proxy-password').val('')
    }
  })
}

$('#save-proxy').click(function () {
  $.ajax({
    url: '/proxies',
    dataType: 'json',
    type: 'post',
    contentType: 'application/json',
    data: JSON.stringify({ 'ip': $('#proxy-ip-addr').val(), 'port': $('#proxy-port').val(), 'user': $('#proxy-user').val(), 'password': $('#proxy-password').val() }),
    processData: false,
    success: function (data, textStatus, jQxhr) {
      loadProxies()
      $('#alert').addClass('alert alert-success').text('proxy added').fadeIn()
      $('#proxy-ip-addr').val('')
      $('#proxy-port').val('')
      $('#proxy-user').val('')
      $('#proxy-password').val('')
    },
    error: function (jqXhr, textStatus, errorThrown) {
      $('#alert').addClass('alert alert-danger').text('something went wrong').fadeIn()
      $('#proxy-ip-addr').val('')
      $('#proxy-port').val('')
      $('#proxy-user').val('')
      $('#proxy-password').val('')
    }
  })
})

$('body').hover(function () {
  $('#alert').removeClass('alert alert-danger').text('').fadeOut('slow')
  $('#alert').removeClass('alert alert-success').text('').fadeOut('slow')
  $('#alert').removeClass('alert alert-warning').text('').fadeOut('slow')
})

$('#save-account').click(function () {
  $.ajax({
    url: '/accounts',
    dataType: 'json',
    type: 'post',
    contentType: 'application/json',
    data: JSON.stringify({ 'name': $('#account-name').val(), 'username': $('#account-username').val(), 'password': $('#account-password').val() }),
    processData: false,
    success: function (data, textStatus, jQxhr) {
      loadAccounts()
      $('#alert').addClass('alert alert-success').text('account added').fadeIn()
      $('#proxy-ip-addr').val('')
      $('#proxy-port').val('')
      $('#proxy-user').val('')
      $('#proxy-password').val('')
    },
    error: function (jqXhr, textStatus, errorThrown) {
      $('#alert').addClass('alert alert-danger').text('something went wrong').fadeIn()
      $('#proxy-ip-addr').val('')
      $('#proxy-port').val('')
      $('#proxy-user').val('')
      $('#proxy-password').val('')
    }
  })
})

function updateTwitterSettings () {
  if ($('#consumer-key').val() && $('#consumer-secret').val() && $('#token').val() && $('#token-secret').val()) {
    $.ajax({
      url: '/configTwitter',
      dataType: 'json',
      type: 'post',
      contentType: 'application/json',
      data: JSON.stringify({ 'TWITTER_CONSUMER_KEY': $('#consumer-key').val(), 'TWITTER_CONSUMER_SECRET': $('#consumer-secret').val(), 'TWITTER_ACCESS_TOKEN': $('#token').val(), 'TWITTER_ACCESS_TOKEN_SECRET': $('#token-secret').val() }),
      processData: false,
      success: function (data, textStatus, jQxhr) {
        loadAccounts()
        $('#alert').addClass('alert alert-success').text('settings updated').fadeIn()
        $('#consumer-key').val('')
        $('#consumer-secret').val('')
        $('#token').val('')
        $('#token-secret').val('')
      },
      error: function (jqXhr, textStatus, errorThrown) {
        $('#alert').addClass('alert alert-danger').text('something went wrong').fadeIn()
        $('#consumer-key').val('')
        $('#consumer-secret').val('')
        $('#token').val('')
        $('#token-secret').val('')
      }
    })
  } else { alert('Input all fields') }
}

loadProxies()
loadTasks()
