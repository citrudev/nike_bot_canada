# base image
FROM ubuntu:latest AS nightmarejs-image

# add all dependencies
# Install MongoDB.
RUN apt-get update
RUN apt-get install -y curl
RUN curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -
RUN apt-get install -y nodejs
RUN apt-get install -y build-essential
RUN apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 7F0CEB10
RUN echo 'deb http://downloads-distro.mongodb.org/repo/ubuntu-upstart dist 10gen' > /etc/apt/sources.list.d/mongodb.list
RUN apt-get update
RUN apt-get install -y mongodb-org 
RUN apt-get install -y git-core
RUN apt-get install -y xvfb 
RUN apt-get install -y x11-xkb-utils 
RUN apt-get install -y xfonts-100dpi
RUN apt-get install -y xfonts-75dpi 
RUN apt-get install -y xfonts-scalable 
RUN apt-get install -y xfonts-cyrillic 
RUN apt-get install -y x11-apps 
RUN apt-get install -y clang
RUN apt-get install -y libdbus-1-dev
RUN apt-get install -y libgtk2.0-dev 
RUN apt-get install -y libnotify-dev 
RUN apt-get install -y libgnome-keyring-dev
RUN apt-get install -y libgconf2-dev 
RUN apt-get install -y libasound2-dev 
RUN apt-get install -y libcap-dev 
RUN apt-get install -y libcups2-dev 
RUN apt-get install -y libxtst-dev
RUN apt-get install -y libxss1 
RUN apt-get install -y libnss3-dev 
RUN apt-get install -y gcc-multilib
RUN apt-get install -y g++-multilib
RUN rm -rf /var/lib/apt/lists/*
RUN systemctl start mongod.service
RUN git config --global user.name "citrudev"
RUN git config --global user.email "evansenonchong@hotmail.com"

# Define mountable directories.
VOLUME ["/data/db"]
WORKDIR /citrudev/app
COPY . ./citrudev/app

# start a bash shell
CMD ["/bin/bash"]

EXPOSE 8080