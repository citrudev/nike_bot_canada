const mongoose = require('mongoose')

module.exports = mongoose.model('task', mongoose.Schema({
  name: { type: String, required: [true, 'name of task required'], trim: true, unique: true, sparse: true },
  type: { type: String, default: 'nike' },
  productUrl: {type: String, default: null},
  sizes: [String],
  session: {type: String, defualt: null},
  username: {type: String},
  password: {type: String},
  twitterHandle: {type: String},
  show: {type: Boolean, default: true}
}))
