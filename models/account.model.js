const mongoose = require('mongoose')

module.exports = mongoose.model('account', mongoose.Schema({
  name: {type: String},
  username: {type: String},
  password: {type: String}
}))
