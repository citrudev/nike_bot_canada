const mongoose = require('mongoose')

module.exports = mongoose.model('proxy', mongoose.Schema({
  ip: { type: String, required: [true, 'Ip address required'], trim: true, unique: true, sparse: true },
  port: { type: String },
  user: { type: String },
  password: { type: String }
}))
