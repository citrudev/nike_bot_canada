let path = require('path')
let helmet = require('helmet')
let dotenv = require('dotenv')
let express = require('express')
let passport = require('passport')
const mongoose = require('mongoose')
let bodyParser = require('body-parser')
let session = require('express-session')
let cookieParser = require('cookie-parser')
const app = express()
let http = require('http').Server(app)
let IO = require('socket.io')(http)
let Twitter = require('./lib/configure.twitter')(require('twit'))

dotenv.config()
mongoose.connect(process.env.MONGODB)
app.locals.auth = require('./lib/configure.authentication')(passport)

app.engine('html', require('hbs').__express)
app.set('views', path.resolve(__dirname, 'front_end'))
app.set('view engine', 'html')
app.use(express.static('front_end/public'))
app.use(express.static('node_modules'))
app.use(helmet())
app.use(helmet.frameguard({ action: 'sameorigin' }))
app.use(helmet.dnsPrefetchControl())
app.use(helmet.hidePoweredBy())
app.use(helmet.ieNoOpen())
app.use(helmet.noSniff())
app.use(helmet.xssFilter())
app.use(bodyParser.json({ extended: true })) // get JSON data
app.use(bodyParser.urlencoded({ extended: true }))
app.use(cookieParser(process.env.COOKIE))
app.use(session({
  secret: process.env.SESSION,
  saveUninitialized: false,
  resave: false
}))
app.use(passport.initialize())
app.use(passport.session())

require('./routes')(app)
require('./controllers/socket.io.controller')(IO, Twitter)

http.listen(process.env.PORT, process.env.IP, () => {
  console.log(`listening on ${process.env.PORT}`)
})
